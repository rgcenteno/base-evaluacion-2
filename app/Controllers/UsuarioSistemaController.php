<?php

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

namespace Com\Daw2\Controllers;

use \Com\Daw2\Helpers\Mensaje;
use \Com\Daw2\Helpers\Utils;

/**
 * Description of UsuarioSistemaController
 *
 * @author rgcenteno
 */
class UsuarioSistemaController extends \Com\Daw2\Core\BaseController{
    
    public function index(?Mensaje $msg = NULL)
    {  
            $_vars = array('titulo' => 'Usuarios sistema',
                          'breadcumb' => array(
                            'Inicio' => array('url' => '#', 'active' => false),
                            'Usuarios sistema' => array('url' => '#','active' => true)),
                           'msg' => $msg,
                            'js' => array('plugins/select2/js/select2.full.min.js', 'assets/js/pages/select2.loader.js')
                );
            //Añade código para que funcione el listado
            $this->view->showViews(array('templates/header.view.php', 'usuariosistema.index.php', 'templates/footer.view.php'), $_vars);      
                
    }      
    
    public function new(){           
            $_vars = [
                'breadcumb' => array(
                            'Inicio' => array('url' => '#', 'active' => false),
                            'UsuariosSistema' => array('url' => './?controller=UsuarioSistema','active' => false),
                            'Insertar' => array('url' => '#','active' => true)),
                          'titulo' => 'Alta usuario sistema',
            ];
            $this->view->showViews(array('templates/header.view.php', 'usuariosistema.edit.view.php', 'templates/footer.view.php'), $_vars);             
    }
    
    public function login(){
        $this->view->showViews(array('login.view.php'), []);   
    }
        
}
