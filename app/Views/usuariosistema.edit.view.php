<?php
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
?>

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-cubes mr-1"></i>
                        Datos usuario sistema
                    </h3>                
                </div>
                <form action="./?controller=usuarioSistema&action=<?php echo $_GET['action']; ?>" method="post">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="nombre">Nombre:</label>
                                    <input type="text" name="nombre" id="nombre" class="form-control" value="" placeholder="Nombre de usuario" />                                    
                                    <p class="text-danger"><small>Error en nombre</small></p>                                    
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="text" name="email" id="email" class="form-control" value="" placeholder="user@domain.org" />                                    
                                    <p class="text-danger"><small>Error en email</small></p>                                   
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Rol: </label>
                                    <select class="form-control" name="id_rol" id="id_rol">
                                        <option value="0"><i>Ninguna</i></option>
                                        <!-- Añadir roles con comprobación de selected -->
                                    </select>                                    
                                    <p class="text-danger"><small>Error en rol</small></p>                                    
                                </div>   
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Password:</label>
                                    <input type="password" name="password" id="password" class="form-control" value="" placeholder="*********" />                                    
                                    <p class="text-danger"><small>Error en password</small></p>                                    
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password2">Reescriba el password:</label>
                                    <input type="password" name="password2" id="password2" class="form-control" value="" placeholder="*********" />                                    
                                    <p class="text-danger"><small>Error en password 2</small></p>                                    
                                </div> 
                            </div>

                        </div></div>
                    <div class="card-footer">                        
                        <a href="./?controller=usuarioSistema" class="btn btn-danger float-right " value="cancelar">Cancelar</a>
                        <button type="submit" name="submit" class="btn btn-primary mr-3 float-right" value="guardar">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



