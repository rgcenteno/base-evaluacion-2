<?php

/* 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
?>
<link rel="stylesheet" href="plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<div class="row">
    <div class="col-12">
        <?php 
        if(!is_null($msg)){
        ?>
        <div class="alert alert-<?php echo $msg->getType(); ?> alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h5><i class="icon <?php echo $msg->getIcon(); ?>"></i> <?php echo $msg->getTitle(); ?></h5>
          <?php echo $msg->getText(); ?>
        </div>
        <?php    
        }
        ?>
        <div class="card shadow mb-4">
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-filter"></i> Filtros</h6>                                    
            </div>
            <form action="/" method="get">
                <input type="hidden" name="controller" value="<?php echo $_GET['controller']; ?>" />
                <input type="hidden" name="action" value="<?php echo isset($_GET['action']) ? $_GET['action'] : ''; ?>" />
                <div class="card-body"> 
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Rol</label>
                                <select class="form-control select2bs4" name="rol[]" multiple="multiple" data-placeholder="Seleccione los roles a buscar">                                    
                                    <!-- Rellenar con registros de roles existentes en bbdd -->
                                    <option value="test1">Test 1</option>
                                    <option value="test2">Test 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="username" name="nombre" placeholder="Nombre de usuario" value="" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="username">Salario min/max</label>
                                <div class="">
                                    <input type="number" class="form-control col-5 float-left" id="min" name="min" placeholder="Min" value="" />

                                    <input type="number" class="form-control col-5 float-right" id="max" name="max" placeholder="Max" value="" />
                                </div>
                            </div>
                        </div>                                                
                    </div>
                    <div class="card-footer">                        
                        <a href="./?controller=UsuarioSistema" class="btn btn-danger float-right " value="reset">Resetear</a>
                        <button type="submit" name="submit" class="btn btn-primary mr-3 float-right" value="filtrar"><i class="fas fa-search"></i> Filtrar</button>
                    </div>                
                </div>
            </form>
        <div class="card shadow mb-4">
            <div
                class="card-header">
                <a href="./?controller=UsuarioSistema&action=new" class="btn btn-outline-primary float-right">Nuevo Usuario</a>                              
            </div>
            <div class="card-body"> 
                <?php 
                $hayRegistros = true;
                    if($hayRegistros){
                ?>
                <table id="usuarioSistemaTable" class="table table-bordered table-striped  dataTable">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>E-mail</th>
                            <th>Rol</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <!-- Para cada registro -->
                        <tr id="usuarioSistema-">
                            <td>Test</td>
                            <td>test@email.org</td>
                            <td>Administador</td>
                            <td align="center"><a class="btn btn-clock btn-outline-primary" href=""><i class="fas fa-edit"></i></a> 
                                <a class="btn btn-clock btn-outline-danger" href=""><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>                            
                    </tbody>
                </table>
                <?php
                }
                else{
                    ?>
                    <div class="callout callout-info">
                      <h5>Sin usuarios del sistema</h5>

                      <p>No existen usuarios del sistema dados de alta que cumplan los requisitos. Pulse aquí para <a href="?controller=usuarioSistema&action=new">crear un nuevo usuario.</a></p>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div> 
</div>
<!--<script src="./vendor/jquery/jquery.min.js"></script>-->
